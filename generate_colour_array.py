from scipy.signal import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import pandas as pd 
from scipy.ndimage import rotate

from PIL import Image
from numpy import asarray
numOfLed=72
img = Image.open("taiga_bw.png")
width = img.width 
height = img.height 

if height != 72: 
    raise Exception("image is not 72 by something")

numpydata = asarray(img)[:, :, 0]
numpydata = rotate(numpydata, angle=270)

numpydata = (numpydata / 32).astype(int)

arrayOZero=np.zeros(numOfLed)
arrayHalf=np.zeros(numOfLed)
arrayFull=np.ones(numOfLed)
f = open("colouredPixel.txt", "w")

f.write("{")

for eachRowInImage in range(width):
    f.write("{")

    for eachZarrayNumber in range(height):
        f.write(str(numpydata[eachRowInImage][eachZarrayNumber]))
        if eachZarrayNumber < 71:
            f.write(", ")
    f.write("},")
    f.write("\n")



f.write("};")
f.close()